<?php

/**
 * @file
 * Search service class for Autonomy IDOL server.
 */

/**
 * Search service class using Autonomy IDOL for storing index information.
 * @todo
 *   - Performance measuring
 */
class SearchApiIDOLService extends SearchApiAbstractService {
  public function supportsFeature($feature) {
    switch (strtolower($feature)) {
      case 'search_api_facets':
      case 'search_api_spellcheck':
      case 'search_api_non_drupal':
        return TRUE;
    }
    return FALSE;
  }

  public function configurationForm(array $form, array &$form_state) {
    if (empty($this->options)) {
      // First configuring this server
      $options = array(
        'host' => 'localhost',
        'port' => '9000',
      );
    }
    else {
      // Editing this server
      $options = $this->options;
    }

    $form['host'] = array(
      '#type' => 'textfield',
      '#title' => t('IDOL server'),
      '#description' => t('The host name or IP of your IDOL server, e.g. <code>localhost</code> or <code>www.example.com</code>.'),
      '#default_value' => $options['host'],
      '#required' => TRUE,
    );
    $form['port'] = array(
      '#type' => 'textfield',
      '#title' => t('IDOL ACI port'),
      '#description' => t('The ACI port of your IDOL server, 9000 by default.'),
      '#default_value' => $options['port'],
      '#required' => TRUE,
    );

    return $form;
  }

  public function configurationFormValidate(array $form, array &$values, array &$form_state) {
    if (isset($values['port']) && (!is_numeric($values['port']) || $values['port'] < 0 || $values['port'] > 65535)) {
      form_error($form['port'], t('The port has to be an integer between 0 and 65535.'));
    }
  }

  public function getNonDrupalIndexes() {
    $params = array('action' => 'getstatus');
    $get_indexes = $this->idolRequest($params);
    return $this->extractIndexes($get_indexes->data);
  }

/**
 * Extracts IDOL databases from a GetStatus action response.
 */
  private function extractIndexes(&$xml_obj) {
    $ignore_indexes = array(
      'iqlrules',
    );

    $indexes = array();
    foreach ($xml_obj->responsedata->databases->database as $database) {
      $index_id = strtolower($database->name);
      $index_name = strtr((string) $database->name, '_', ' ');

      if (strpos($index_id, 'drupal_') !== 0 && !in_array($index_id, $ignore_indexes)) {
        $indexes[$index_id] = $index_name;
      }
    }

    return $indexes;
  }

  public function getNonDrupalProperties($database) {
// @todo
// - Cache?
// - Better way to get all fields?
    $params = array(
      'action' => 'query',
      'text' => '*',
      'maxresults' => 10,
      'databasematch' => $database,
    );
    $get_fields = $this->idolRequest($params);
    return $this->extractFields($get_fields->data);
  }

  private function extractFields(&$xml_obj) {
    $field_name_map = array(
      'drereference' => 'Reference (URL)',
      'dretitle' => 'Title',
      'dredate' => 'Index Date',
      'dredbname' => 'Database',
      'drecontent' => 'Content',
    );

    $fields = array();
    foreach ($xml_obj->xpath('responsedata/autn:hit/autn:content/DOCUMENT/*') as $field) {
      $field_id = strtolower($field->getName());
      $field_label = str_replace('_', ' ', $field_id);
      if (in_array($field_id, array_keys($field_name_map))) {
        $field_label = $field_name_map[$field_id];
      }

      $fields[$field_id] = array(
        'id' => $field_id,
        'label' => $field_label,
        'description' => '// @todo',
        'type' => 'string',
      );
    }

    return $fields;
  }

  public function indexItems(SearchApiIndex $index, array $items) {
// @todo - Support.
    throw new SearchApiException(t('Autonomy IDOL module cannot update indexes.'));
  }

  public function deleteItems($ids = 'all', SearchApiIndex $index = NULL) {
// @todo - Support.
    throw new SearchApiException(t('Autonomy IDOL module cannot update indexes.'));
  }

  public function search(SearchApiQueryInterface $query) {
    $index = $query->getIndex();

    $offset = (int) $query->getOption('offset');
    $limit = (int) $query->getOption('limit');
    $maxresults = $offset + $limit;

    $indexed_fields = array();
    foreach ($index->options['fields'] as $field_id => $field) {
      if ($field['indexed']) {
        $indexed_fields[$field_id] = $field;
      }
    }

    $params = array(
      'action' => 'query',
	  'MatchEncoding' => 'UTF8',
      'combine' => 'simple',
      'totalresults' => 'true',
      'predict' => 'false',
      'start' => $offset + 1,
      'maxresults' => $maxresults,

//@todo - Integrate with search_api_spellcheck.
      'spellcheck' => 'true',

// @todo
//   - Deal with Drupal and non databases.
//   - Context all Databases with drupal_*.
      'databasematch' => $index->machine_name == 'all'?'':$index->machine_name,

      'text' => $this->createKeywordsQuery($query),
      'fieldtext' => $this->createFiltersQuery($query),

// @todo - Don't get all fields.
//      'printfields' => implode(',', array_keys($indexed_fields)),

// @todo - User configurable settings?
      'synonym' => 'true',
      'summary' => 'context',
	  'characters' => '350',

      'highlight' => 'terms+summaryterms',
      'starttag' => '<strong>',
      'endtag' => '</strong>',
    );

// @todo - Move to controller as ->condition().
// Add a strong bias for crawled/web results
    if ($params['fieldtext'] != '') $params['fieldtext'] .= ' AND ';
    $params['fieldtext'] .= 'BIAS{9999999999,9999999999,15}:SPIDERDATE';

    foreach ($query->getSort() as $field => $order) {
      switch (strtolower($order)) {
        case 'asc':
          $params['sort'][] = $field . ':increasing';
          break;
        case 'desc':
          $params['sort'][] = $field . ':decreasing';
          break;
      }
    }

    if (isset($params['sort'])) {
      $params['sort'] = implode(' ', $params['sort']);
    }

    if ($maxresults >= 1) {
      $get_results = $this->idolRequest($params);
      $results = $this->extractResults($get_results->data, $indexed_fields);

      $results['idol_requests']['query'] = $get_results->request_url;

      if ($results['result count'] > 0 && $offset >= $results['result count']) {
        $first_query = $results['idol_requests']['query'];

        $offset = $limit * (ceil($results['result count']/$limit) - 1);
        $maxresults = $offset + $limit;
        $params['start'] = $offset + 1;
        $params['maxresults'] = $maxresults;

        $get_results = $this->idolRequest($params);
        $results = $this->extractResults($get_results->data, $indexed_fields);

        $results['idol_requests']['query'] = $first_query;
        $results['idol_requests']['page_corrected_query'] = $get_results->request_url;
      }

    }
    else {
      $results = array(
        'results' => array()
      );
    }

    if ($facets = $query->getOption('search_api_facets')) {
      $params['action'] = 'getquerytagvalues';
      $params['documentcount'] = 'true';
      unset($params['maxresults']);

      $params['fieldname'] = array();
      foreach ($facets as $id => $facet) {
        $params['fieldname'][] = $facet['field'];
      }
      $params['fieldname'] = implode(',', $params['fieldname']);

      $get_facets = $this->idolRequest($params);
      $results['search_api_facets'] = $this->extractFacets($get_facets->data, $facets);

      $results['idol_requests']['facets'] = $get_facets->request_url;
    }

    $results['offset'] = $offset;
    $results['limit'] = $limit;

    return $results;
  }

  private function formatQuery($query, $conjunction = 'AND') {
	$query = preg_replace('/(\\:|\\/|\\[|\\])/isD', '\\\$1', $query);
	$query = preg_replace('/-+/isD', '-', $query);

    preg_match_all('/(?<=^|\\s)\\S+(?=\\s|$)/isD', $query, $query);
    $query = array_shift($query);
    $formatted_query = '';

    $quoted = FALSE;
    foreach ($query as $term) {
      $ignored = FALSE;
      if (preg_match('/^and|or|not$/isD', $term)) {
        if ($formatted_query != '') {
          $formatted_query .= ' ';
        }
        $formatted_query .= strtoupper($term);
      }
      // Ignore essentially empty terms.
      elseif (preg_match('/[a-z0-9]/isD', $term)) {
        if ($formatted_query != '') {
          $formatted_query .= ' ';

          if (!preg_match('/(?:and|or|not)\\s+$/isD', $formatted_query) && !$quoted) {
            $formatted_query .= $conjunction . ' ';
          }
        }

        if (preg_match('/^[^"]+-[^"]+$/isD', $term) && !$quoted) {
          $term = '"' . $term . '"';
        }
        $formatted_query .= $term;
      }
      else {
        $ignored = TRUE;
      }

      if (preg_match('/^(\(*").*[^"]$/isD', $term, $opening_quote)) {
        $quoted = TRUE;
        // If term was ignored, add the opening quote.
        if ($ignored) {
          $formatted_query .= $opening_quote[1];
        }
      }
      elseif (preg_match('/("\)*)$/isD', $term, $closing_quote)) {
        $quoted = FALSE;
        // If term was ignored, add the closing quote.
        if ($ignored) {
          $formatted_query .= $closing_quote[1];
        }
      }
    }

    return $formatted_query;
  }

  private function formatFilterValue($value) {
    if (!is_array($value)) {
      $value = array($value);
    }
	$value = preg_replace('/,/isD', '%2c', $value);

    return implode(',', $value);
  }

  private function idolRequest(&$params) {
    $request_url = 'http://' . $this->options['host'] . ':' . $this->options['port'] . '/?' . drupal_http_build_query($params);

    $request = drupal_http_request($request_url, array('max_redirects' => 0));
    $request->request_url = $request_url;

    if ($request->code != 200) {
      throw new SearchApiException(t(
        'Autonomy IDOL server @server returned HTTP code @code when retrieving @url.',
        array(
          '@server' => $this->name . ' (' . $this->options['host'] . ':' . $this->options['port'] . ')',
          '@code' => $request->code,
          '@url' => $request_url,
        )
      ));
    }

    if (!$request->data = simplexml_load_string($request->data)) {
      throw new SearchApiException(t(
        'Autonomy IDOL server @server returned non-parsable XML in request @url.',
        array(
          '@server' => $this->name . ' (' . $this->options['host'] . ':' . $this->options['port'] . ')',
          '@url' => $request_url,
        )
      ));
    }

    if (strtolower($request->data->response) != 'success') {
      throw new SearchApiException(t(
        'Autonomy IDOL server @server returned ACI response: @response in request !url.',
        array(
          '@server' => $this->name . ' (' . $this->options['host'] . ':' . $this->options['port'] . ')',
          '@response' => $request->data->response,
          '!url' => $request_url,
        )
      ));
    }

    return $request;
  }

  private function extractResults(&$xml_obj, $fields) {
    $xml_obj = $xml_obj->responsedata;
    $results = array(
      'result count' => (int) array_shift($xml_obj->xpath('autn:totalhits')),
      'results' => array(),
    );

    $spelling_suggestion = (string) array_shift($xml_obj->xpath('autn:spellingquery'));
    if ($spelling_suggestion != '') {
// @todo - Lower case keywords (except AND/OR/NOT)
      $results['search_api_spellcheck'] = $spelling_suggestion;
    }
// @todo - Standardise as below.
/*    if (module_exists('search_api_spellcheck') && $query->getOption('search_api_spellcheck')) {
      $results['search_api_spellcheck'] = new SearchApiSpellcheckSolr($response);
    }*/

    foreach ($xml_obj->xpath('autn:hit') as $hit) {
      $id = strtolower(array_shift($hit->xpath('autn:reference')));

// @todo - Remove when data is correct.
      // Add "http://" if not already there.
      if (!preg_match('/^https?\\:\\/\\//isD', $id)) {
        $id = 'http://' . $id;
      }

      $results['results'][$id] = array(
        'id' => $id,
        'url' => $id,
        'score' => (float) array_shift($hit->xpath('autn:weight')),
        'summary' => (string) array_shift($hit->xpath('autn:summary')),
        'database' => strtolower(array_shift($hit->xpath('autn:database'))),
        'fields' => array(),
      );

      foreach ($fields as $field_id => $field) {
        switch ($field['type']) {
          default:
// @todo - Remove trim when data is correct.
            $results['results'][$id]['fields'][$field_id] = trim((string) array_shift($hit->xpath('autn:content/DOCUMENT/' . strtoupper($field_id))));
        }
      }

// @todo - Don't dump all fields.
      foreach ($hit->xpath('autn:content/DOCUMENT/*') as $field) {
        $results['results'][$id]['fields']['idol_fields'][$field->getName()] = (string) $field;
      }
    }

    return $results;
  }

  private function extractFacets(&$xml_obj, $facets_info = array()) {
    $facet_results = $xml_obj->xpath('//autn:field');

    $facet_info_default = array(
      'limit' => 50,
      'min_count' => 1,
      'missing' => TRUE,
      'operator' => 'and',
    );

    $facets = array();
    foreach ($facet_results as $result) {
      $field = strtr(strtolower(array_shift($result->xpath('autn:name'))), array('document/' => ''));
      if (!isset($facets_info[$field])) {
        $facets_info[$field] = array();
      }
      $facet_info = array_merge($facet_info_default, $facets_info[$field]);

      $result_values = $result->xpath('autn:value');

      $values = array();
      foreach ($result_values as $value) {
        if ($facet_info['limit'] > 0 && count($values) == $facet_info['limit']) {
          break;
        }

        $count = (int) $value['count'];
        $filter = strtolower($value);
        if (($filter != '' || $facet_info['missing']) && $count >= $facet_info['min_count']) {
          $values[] = array(
            'count' => $count,
            'filter' => $filter == ''?'!':$filter,
          );
        }
      }

      $facets[$field] = $values;
    }

    return $facets;
  }

  protected function createKeywordsQuery(SearchApiQueryInterface $query) {
    $index = &$query->getIndex();

    $keywords = array(
      $this->formatQuery($query->getOriginalKeys()),
    );

    $filters = array();
    foreach ($query->getFilter()->getFilters() as $filter) {
      if ($filter[1] != '') {
// @todo
//   - Support '<>' filter.
//   - Support $filter as an array.
        foreach (explode(':', $filter[0]) as $filter_field) {
          if (!in_array($filter_field, $index->getFulltextFields())) {
            continue 2;
          }
        }

        $keywords[] = '(' . $this->formatQuery($filter[1]) . '):/*' . $filter[0];
      }
    }

    $keywords = array_filter($keywords);

    if (count($keywords) == 0) {
      return '';
    }

    return '(' . implode(') AND (', $keywords) . ')';
  }

  protected function createFiltersQuery(SearchApiQueryInterface $query) {
    $index = &$query->getIndex();

    $filters = array();
    foreach ($query->getFilter()->getFilters() as $filter) {
      foreach (explode(':', $filter[0]) as $filter_field) {
        if (!in_array($filter_field, $index->getFulltextFields())) {
          if (!in_array($filter[0], $index->getFulltextFields())) {
            if (is_array($filter)) {
              if ($filter[1] != '') {
                $filters[] = $this->createFilterQuery($filter);
              }
            }
            else {
              $filters[] = $this->createFiltersQuery($filter);
            }
          }
          break;
        }
      }
    }

    if (count($filters) == 0) {
      return '';
    }

    return '(' . implode(') ' . $query->getFilter()->getConjunction() . ' (', $filters) . ')';
  }

  protected function createFilterQuery(&$filter) {
    $value = $this->formatFilterValue($filter[1]);
    switch ($filter[2]) {
/*      case '<>':
        return "-($field:$value)";
      case '<':
        return "$field:{* TO $value}";
      case '<=':
        return "$field:[* TO $value]";
      case '>=':
        return "$field:[$value TO *]";
      case '>':
        return "$field:{{$value} TO *}";*/

      default:
        return 'MATCH{' . $value . '}:' . $filter[0];
    }
  }
}
